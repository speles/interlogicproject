﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Server.REST.DTO
{
    public class QuestionDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = true)]
        [Display(Name = "Text")]
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}