﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Server.REST.DTO
{
    public class AnswerDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = true)]
        [Display(Name = "Answer text")]
        [JsonProperty("text")]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Is answer right?")]
        [UIHint("IsRight")]
        [JsonProperty("is_right")]
        public bool IsRight { get; set; }
    }
}