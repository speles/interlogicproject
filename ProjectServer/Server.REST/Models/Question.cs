﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.REST.Models
{
    public class Question
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public virtual ICollection<Answer> QuestionAnswers { get; set; }
        public Question()
        {
            QuestionAnswers = new List<Answer>();
        }
    }
}