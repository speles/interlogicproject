﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.REST.Models
{
    interface ITestsRepository
    {
        void Add(Test test);
        IEnumerable<Test> GetAll();
        Test Find(int key);
        Test Remove(int key);
        void Update(int key, Test test);
    }
}
