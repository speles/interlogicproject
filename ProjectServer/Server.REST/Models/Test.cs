﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Server.REST.Models
{
    public class Test
    {
        public int TestId { get; set; }
        public string TestName { get; set; }
        public virtual ICollection<Question> TestQuestions { get; set; }

        public Test()
        {
            TestQuestions = new List<Question>();
        }
    }
}