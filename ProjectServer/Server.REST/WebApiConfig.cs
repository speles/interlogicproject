﻿using AutoMapper;
using Server.REST.DTO;
using Server.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Server.REST
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            Mapper.Initialize(cfg =>
            {
                cfg.RecognizeDestinationPrefixes("Question", "Answer", "Test");
                cfg.RecognizePrefixes("Question", "Answer", "Test");

                cfg.CreateMap<Answer, AnswerDTO>();
                cfg.CreateMap<AnswerDTO, Answer>();

                cfg.CreateMap<Question, QuestionDTO>();
                cfg.CreateMap<QuestionDTO, Question>();

                cfg.CreateMap<Test, TestDTO>();
                cfg.CreateMap<TestDTO, Test>();
            });

            config.Routes.MapHttpRoute(
                name: "AnswersApi",
                routeTemplate: "api/Tests/{testId}/Questions/{questionId}/Answers/{id}",
                defaults: new { controller = "Answers", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "QuestionsApi",
                routeTemplate: "api/Tests/{testId}/Questions/{id}",
                defaults: new { controller = "Questions", id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/Tests/{id}/{action}",
                defaults: new { controller = "Tests", id = RouteParameter.Optional, action = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
