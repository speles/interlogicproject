﻿using Server.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Server.REST.Services;
using System.Web.Http;
using Server.REST.DTO;
using AutoMapper;

namespace Server.REST.Controllers
{
    public class QuestionsController : ApiController
    {
        private TestsContext testsRepository;

        public QuestionsController()
        {
            this.testsRepository = new TestsContext();
        }

        // GET: api/Tests/5/Questions/
        public IEnumerable<QuestionDTO> Get(int testId)
        {
            return Mapper.Map<IEnumerable<QuestionDTO>>(testsRepository.Find(testId).TestQuestions);
        }

        // GET: api/Tests/5/Questions/5/
        public QuestionDTO Get(int testId, int id)
        {
            return Mapper.Map<QuestionDTO>(testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == id));
        }

        // POST: api/Tests/5/Questions/
        public void Post(int testId, [FromBody]QuestionDTO questionData)
        {
            testsRepository.Find(testId).TestQuestions.Add(Mapper.Map<Question>(questionData));
            testsRepository.SaveChanges();
        }

        // PUT: api/Tests/5/Questions/5/
        public void Put(int testId, int id, [FromBody]QuestionDTO questionData)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == id);
            question.QuestionText = questionData.Text;
            testsRepository.SaveChanges();
        }

        // DELETE: api/Tests/5/Questions/5/
        public void Delete(int testId, int id)
        {
            Test test = testsRepository.Find(testId);
            test.TestQuestions.Remove(test.TestQuestions.FirstOrDefault(q => q.QuestionId == id));
            testsRepository.SaveChanges();
        }
    }
}
