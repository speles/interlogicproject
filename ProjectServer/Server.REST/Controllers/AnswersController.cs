﻿using Server.REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Server.REST.Services;
using System.Web.Http;
using Server.REST.DTO;
using AutoMapper;

namespace Server.REST.Controllers
{
    public class AnswersController : ApiController
    {
        private TestsContext testsRepository;

        public AnswersController()
        {
            this.testsRepository = new TestsContext();
        }

        // GET: api/Tests/5/Questions/5/Answers/
        public IEnumerable<AnswerDTO> Get(int testId, int questionId)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == questionId);
            return Mapper.Map<List<AnswerDTO>>(question.QuestionAnswers);
        }

        // GET: api/Tests/5/Questions/5/Answers/5
        public AnswerDTO Get(int testId, int questionId, int id)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == questionId);
            return Mapper.Map<AnswerDTO>(question.QuestionAnswers.FirstOrDefault(a => a.AnswerId == id));
        }

        // POST: api/Tests/5/Questions/5/Answers/
        public void Post(int testId, int questionId, [FromBody]AnswerDTO answerData)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == questionId);
            question.QuestionAnswers.Add(Mapper.Map<Answer>(answerData));
            testsRepository.SaveChanges();
        }

        // PUT: api/Tests/5/Questions/5/Answers/5
        public void Put(int testId, int questionId, int id, [FromBody]AnswerDTO answerData)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == questionId);
            Answer answer = question.QuestionAnswers.FirstOrDefault(a => a.AnswerId == answerData.Id);
            answer.AnswerIsRight = answerData.IsRight;
            answer.AnswerText = answerData.Text;
            testsRepository.SaveChanges();
        }

        // DELETE: api/Tests/5/Questions/5/
        public void Delete(int testId, int questionId, int id)
        {
            Question question = testsRepository.Find(testId).TestQuestions.FirstOrDefault(q => q.QuestionId == questionId);
            question.QuestionAnswers.Remove(question.QuestionAnswers.FirstOrDefault(a => a.AnswerId == id));
            testsRepository.SaveChanges();
        }

    }
}
