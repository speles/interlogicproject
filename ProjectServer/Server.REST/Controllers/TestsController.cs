﻿using AutoMapper;
using Server.REST.DTO;
using Server.REST.Models;
using Server.REST.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Server.REST.Controllers
{
    public class TestsController : ApiController
    {
        private TestsContext testsRepository;

        public TestsController()
        {
            this.testsRepository = new TestsContext();
        }

        // GET: api/Tests
        public IEnumerable<TestDTO> Get()
        {
            return Mapper.Map<IEnumerable<TestDTO>>(testsRepository.GetAll());
        }

        // GET: api/Tests/5
        public TestDTO Get(int id)
        {
            return Mapper.Map<TestDTO>(testsRepository.Find(id));
        }

        // POST: api/Tests
        public void Post([FromBody]TestDTO test)
        {
            testsRepository.Add(Mapper.Map<Test>(test));
        }

        // PUT: api/Tests/5
        public void Put(int id, [FromBody]TestDTO test)
        {
            testsRepository.Update(id, Mapper.Map<Test>(test));
        }

        // DELETE: api/Tests/5
        public void Delete(int id)
        {
            testsRepository.Remove(id);
        }
        [HttpPost]
        public object Check(int id, [FromBody]KeyValuePair<int, int[]>[] userAnswers)
        {
            Test test = testsRepository.Find(id);
            if (test == null)
                return new { Message = "Test not found" };
            int result = 0;
            foreach (var userAnswer in userAnswers)
            {
                if (userAnswer.Key >= 0 && test.TestQuestions.Count > userAnswer.Key)
                {
                    var question = test.TestQuestions.ElementAt(userAnswer.Key);
                    bool right = true;
                    for (int i = 0; i < question.QuestionAnswers.Count; i++)
                    {
                        var questionAnswer = question.QuestionAnswers.ElementAt(i);
                        if (questionAnswer.AnswerIsRight)
                        {
                            if (!userAnswer.Value.Contains(i))
                                right = false;
                        } else
                        {
                            if (userAnswer.Value.Contains(i))
                                right = false;
                        }
                    }
                    if (right)
                        result++;
                }
            }
            return new { Result = result };
        }
    }
}
