﻿using Server.REST.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Server.REST.Services
{
    public class TestsContext: DbContext
    {
        public TestsContext(): base("DbConnection")
        {
            Database.SetInitializer<TestsContext>(new DropCreateDatabaseIfModelChanges<TestsContext>());
            Tests.Load();
            Questions.Load();
            Answers.Load();
            MyUsers.Load();
        }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<MyUser> MyUsers { get; set; }
        public void Add(Test test)
        {
            Tests.Add(test);
            SaveChanges();
        }

        public IEnumerable<Test> GetAll()
        {
            return Tests.Local;
        }

        public Test Find(int key)
        {
            return Tests.Local.FirstOrDefault(t => t.TestId == key);
        }

        public Test Remove(int key)
        {
            
            var test = Tests.Local.FirstOrDefault(t => t.TestId == key);
            Tests.Remove(test);
            SaveChanges();
            return test;
        }

        public void Update(int key, Test test)
        {
            Tests.Local.FirstOrDefault(t => t.TestId == key).TestName = test.TestName;
            SaveChanges();
        }
    }
}