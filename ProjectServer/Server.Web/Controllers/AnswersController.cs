﻿using Server.REST.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Server.Web.Controllers
{
    public class AnswersController : Controller
    {
        REST.Controllers.AnswersController restAnswersContoller = DependencyResolver.Current.GetService<REST.Controllers.AnswersController>();

        // GET: Tests/5/Questions/5/Answers
        public ActionResult Index(int testId, int questionId)
        {
            IEnumerable<AnswerDTO> answers = restAnswersContoller.Get(testId, questionId);
            return View(answers);
        }
        // GET: Tests/5/Questions/Create
        public ActionResult Create(int testId, int questionId)
        {
            return View();
        }

        // POST: Tests/5/Questions/5/Answers/Create
        [HttpPost]
        public ActionResult Create(int testId, int questionId, AnswerDTO answer)
        {
            if (ModelState.IsValid)
            {
                restAnswersContoller.Post(testId, questionId, answer);
                return RedirectToAction("Index");
            }
            return View(answer);
        }

        // GET: Tests/5/Questions/5/Answers/Edit/5
        public ActionResult Edit(int testId, int questionId, int id)
        {
            AnswerDTO answer = restAnswersContoller.Get(testId, questionId, id);
            return View(answer);
        }

        // POST: Tests/5/Questions/5/Answers/Edit/5
        [HttpPost]
        public ActionResult Edit(int testId, int questionId, int id, AnswerDTO answer)
        {
            if (ModelState.IsValid)
            {
                restAnswersContoller.Put(testId, questionId, id, answer);
                return RedirectToAction("Index");
            }
            return View(answer);
        }

        // GET: Tests/5/Questions/5/Answers/Delete/5
        public ActionResult Delete(int testId, int questionId, int id)
        {
            return View();
        }

        // POST: Tests/5/Questions/5/Answers/Delete/5
        [HttpPost]
        public ActionResult Delete(int testId, int questionId, int id, FormCollection collection)
        {
            restAnswersContoller.Delete(testId, questionId, id);
            return RedirectToAction("Index");
        }
    }

}