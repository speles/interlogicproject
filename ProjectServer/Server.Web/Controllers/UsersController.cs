﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Server.REST.Models;
using System.Web.Security;
using Server.REST.Services;
using System.Data.Entity.Validation;

namespace Server.Web.Controllers
{
    public class UsersController : Controller
    {
        private TestsContext db = new TestsContext();
        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(MyUser user)
        {
            if (IsValid(user.Login, user.Password))
            {
                FormsAuthentication.SetAuthCookie(user.Login, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Login details are wrong.");
            }
            return View(user);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(MyUser user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newUser = db.MyUsers.Create();
                    newUser.Login = user.Login;
                    newUser.Password = user.Password;
                    db.MyUsers.Add(newUser);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Data is not correct");
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private bool IsValid(string login, string password)
        {
            var user = db.MyUsers.FirstOrDefault(u => u.Login == login);
            if (user != null)
            {
                if (user.Password == password)
                {
                    return true;
                }
            }
            return false;
        } 
    }
}
