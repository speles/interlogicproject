﻿using Newtonsoft.Json;
using Server.REST.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Server.Web.Controllers
{
    public class QuestionsController : Controller
    {
        REST.Controllers.QuestionsController restQuestionsContoller = DependencyResolver.Current.GetService<REST.Controllers.QuestionsController>();

        // GET: Tests/5/Questions
        public ActionResult Index(int testId)
        {
            IEnumerable<QuestionDTO> questions = restQuestionsContoller.Get(testId);
            return View(questions);
        }
        // GET: Tests/5/Questions/Create
        public ActionResult Create(int testId)
        {
            return View();
        }

        // POST: Tests/5/Questions/Create
        [HttpPost]
        public ActionResult Create(int testId, QuestionDTO question)
        {
            if (ModelState.IsValid)
            {
                restQuestionsContoller.Post(testId, question);
                return RedirectToAction("Index");
            }
            return View(question);
        }

        // GET: Tests/5/Questions/Edit/5
        public ActionResult Edit(int testId, int id)
        {
            QuestionDTO question = restQuestionsContoller.Get(testId, id);
            return View(question);
        }

        // POST: Tests/5/Questions/Edit/5
        [HttpPost]
        public ActionResult Edit(int testId, int id, QuestionDTO question)
        {
            if (ModelState.IsValid)
            {
                restQuestionsContoller.Put(testId, id, question);
                return RedirectToAction("Index");
            }
            return View(question);
        }

        // GET: Tests/5/Questions/Delete/5
        public ActionResult Delete(int testId, int id)
        {
            return View();
        }

        // POST: Tests/5/Questions/Delete/5
        [HttpPost]
        public ActionResult Delete(int testId, int id, FormCollection collection)
        {
            restQuestionsContoller.Delete(testId, id);
            return RedirectToAction("Index");
        }
    }
}
