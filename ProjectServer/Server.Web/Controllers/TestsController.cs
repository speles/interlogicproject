﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.qrcode;
using Newtonsoft.Json;
using Server.REST.DTO;
using Server.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Server.Web.Controllers
{
    public class TestsController : Controller
    {
        REST.Controllers.TestsController restTestsContoller = DependencyResolver.Current.GetService<REST.Controllers.TestsController>();

        // GET: Tests
        public ActionResult Index()
        {
            IEnumerable<TestDTO> tests = restTestsContoller.Get();
            return View(tests);
        }
        // GET: Tests/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tests/Create
        [HttpPost]
        public ActionResult Create(TestDTO test)
        {
            if (ModelState.IsValid)
            {
                restTestsContoller.Post(test);
                return RedirectToAction("Index");
            }
            return View(test);
        }

        // GET: Tests/Edit/5
        public ActionResult Edit(int id)
        {
            TestDTO test = restTestsContoller.Get(id);
            return View(test);
        }

        // POST: Tests/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TestDTO test)
        {
            if (ModelState.IsValid)
            {
                restTestsContoller.Put(id, test);
                return RedirectToAction("Index");
            }
            return View(test);
        }

        // GET: Tests/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Tests/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            restTestsContoller.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Print(int id)
        {
            TestDTO test = restTestsContoller.Get(id);
            REST.Controllers.QuestionsController restQuestionsContoller = DependencyResolver.Current.GetService<REST.Controllers.QuestionsController>();
            REST.Controllers.AnswersController restAnswersContoller = DependencyResolver.Current.GetService<REST.Controllers.AnswersController>();

            MemoryStream pdfMemoryStream = new MemoryStream();
            Document doc = new Document();
            PdfWriter.GetInstance(doc, pdfMemoryStream);
            doc.Open();

            var normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);
            var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 16);
            var obliqueFont = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 12);

            var nameParagraph = new Paragraph();
            nameParagraph.Add(new Chunk(test.Name, boldFont));
            nameParagraph.Alignment = Element.ALIGN_CENTER;
            doc.Add(nameParagraph);

            foreach (var question in restQuestionsContoller.Get(id))
            {
                Paragraph paragraph = new Paragraph();
                paragraph.Add(new Chunk(question.Text, normalFont));
                List list = new List(true, true);
                list.IndentationLeft = 20f;
                foreach (var answer in restAnswersContoller.Get(id, question.Id))
                {
                    list.Add(answer.Text);
                }
                doc.Add(paragraph);
                doc.Add(list);
            }
            doc.Close();
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = $"Test{test.Id}.pdf",
                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(pdfMemoryStream.ToArray(), "application/pdf");
        }

        [HttpGet]
        public ActionResult PrintBlank(int id)
        {
            TestDTO test = restTestsContoller.Get(id);
            REST.Controllers.QuestionsController restQuestionsContoller = DependencyResolver.Current.GetService<REST.Controllers.QuestionsController>();
            REST.Controllers.AnswersController restAnswersContoller = DependencyResolver.Current.GetService<REST.Controllers.AnswersController>();

            int questionsCount = restQuestionsContoller.Get(id).Count();
            string qrText = JsonConvert.SerializeObject(new { test_id = id, user_id = 1, test = "aaa" });
            MemoryStream pdfMemoryStream = BlankBuilderHelper.BuildBlank(questionsCount, qrText, $"Test: {test.Name}");

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = $"TestBlank{test.Id}.pdf",
                Inline = true,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(pdfMemoryStream.ToArray(), "application/pdf");
        }
    }
}
