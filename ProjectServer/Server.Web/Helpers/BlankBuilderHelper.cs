﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.qrcode;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Server.Web.Helpers
{
    public class BlankBuilderHelper
    {
        static Font normalFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);
        static Font boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 16);
        static Font obliqueFont = FontFactory.GetFont(FontFactory.HELVETICA_OBLIQUE, 12);

        private static Image GenerateQR(PdfWriter writer, string text)
        {
            IDictionary<EncodeHintType, Object> hints = new Dictionary<EncodeHintType, object>();
            hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BarcodeQRCode qrCode = new BarcodeQRCode(text, 200, 200, hints);
            Image img = qrCode.GetImage();
            float imgWidth = img.ScaledWidth;
            float imgHeight = img.ScaledHeight;
            const int CROP_SIZE = 25;
            PdfTemplate cropTemplate = writer.DirectContent.CreateTemplate(imgWidth - CROP_SIZE * 2, imgHeight - CROP_SIZE * 2);
            cropTemplate.AddImage(img, imgWidth, 0, 0, imgHeight, -CROP_SIZE, -CROP_SIZE);
            img = Image.GetInstance(cropTemplate);
            return img;
        }

        private static PdfPTable GenerateHeader(PdfWriter writer, string qrText, string headerText)
        {

            PdfPTable headerTable = new PdfPTable(2);
            var headerWidthPercentage = new[] { 40, 60f };
            headerTable.SetWidths(headerWidthPercentage);

            PdfPCell qrCell = new PdfPCell(GenerateQR(writer, qrText), false);
            qrCell.BorderWidth = 4;
            qrCell.Border = PdfPCell.BOX;
            qrCell.BorderColor = BaseColor.BLACK;
            qrCell.Padding = 5;
            qrCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            qrCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            headerTable.AddCell(qrCell);

            PdfPCell testInfoCell = new PdfPCell(new Paragraph(headerText, boldFont));
            testInfoCell.Border = PdfPCell.NO_BORDER;
            testInfoCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            testInfoCell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
            headerTable.AddCell(testInfoCell);

            return headerTable;

        }

        private static PdfPTable GenerateTitle()
        {
            PdfPTable titleTable = new PdfPTable(5);
            titleTable.SpacingAfter = 10;
            titleTable.DefaultCell.Border = PdfPCell.NO_BORDER;
            titleTable.DefaultCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            titleTable.DefaultCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

            titleTable.AddCell("");
            const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < 4; i++)
                titleTable.AddCell(new Phrase(alphabet[i].ToString(), boldFont));

            return titleTable;
        }

        private static PdfPTable GenerateQuestionField(PdfWriter writer, int number)
        {
            PdfPTable table = new PdfPTable(5);
            table.SpacingAfter = 20;

            PdfPCell numberCell = new PdfPCell(new Phrase(number.ToString(), boldFont));
            numberCell.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER;
            numberCell.BorderColor = BaseColor.BLACK;
            numberCell.BorderWidth = 4;
            numberCell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            numberCell.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
            table.AddCell(numberCell);

            const int SQUARE_SIZE = 20;
            PdfTemplate template = writer.DirectContent.CreateTemplate(SQUARE_SIZE, SQUARE_SIZE);
            template.SetLineWidth(6);
            template.SetColorStroke(BaseColor.BLACK);
            template.Rectangle(0, 0, SQUARE_SIZE, SQUARE_SIZE);
            template.Stroke();
            writer.ReleaseTemplate(template);

            for (int i = 0; i < 4; i++)
            {
                PdfPCell cell = new PdfPCell(Image.GetInstance(template));
                cell.Padding = 8;
                cell.Border = PdfPCell.TOP_BORDER | PdfPCell.BOTTOM_BORDER;
                if (i == 3)
                    cell.Border |= PdfPCell.RIGHT_BORDER;

                cell.BorderColor = BaseColor.BLACK;
                cell.BorderWidth = 4;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                table.AddCell(cell);
            }
            return table;
        }

        public static MemoryStream BuildBlank(int questionsCount, string qrText, string headerText)
        {
            MemoryStream pdfMemoryStream = new MemoryStream();
            Document doc = new Document();
            PdfWriter writer = PdfWriter.GetInstance(doc, pdfMemoryStream);
            doc.Open();

            var cb = writer.DirectContent;
            cb.SetLineWidth(4f);
            cb.Rectangle(60f, 20f, doc.PageSize.Width - 120f, doc.PageSize.Height - 40f);

            doc.Add(GenerateHeader(writer, qrText, headerText));
            
            doc.Add(GenerateTitle());

            for (int questionIndex = 1; questionIndex <= questionsCount; questionIndex++)
            {
                doc.Add(GenerateQuestionField(writer, questionIndex));
            }
            doc.Close();
            return pdfMemoryStream;
        }
    }
}