﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Android.Views;
using OpenCV.Android;
using OpenCV.Core;

#pragma warning disable 0618

namespace MyProject
{
	[Activity(
		Label = "MyProject", 
		MainLauncher = true, 
		Icon = "@mipmap/icon",
	    ScreenOrientation = ScreenOrientation.Landscape,
		ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation)]
	public class MainActivity: Activity, CameraBridgeViewBase.ICvCameraViewListener2, View.IOnTouchListener
	{
		public const string TAG = "MyProject::MainActivity";

		public CameraControlView mOpenCvCameraView { get; private set; }

		private BaseLoaderCallback mLoaderCallback;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);
			mLoaderCallback = new Callback(this, this);
			Window.AddFlags(WindowManagerFlags.KeepScreenOn);
			SetContentView(Resource.Layout.Main);

			mOpenCvCameraView = FindViewById<CameraControlView>(Resource.Id.camera_surface_view);
			mOpenCvCameraView.Visibility = ViewStates.Visible;
			mOpenCvCameraView.SetCvCameraViewListener2(this);
		}

		protected override void OnPause()
		{
			base.OnPause();
			if (mOpenCvCameraView != null)
				mOpenCvCameraView.DisableView();
		}

		protected override void OnResume()
		{
			base.OnResume();
			if (!OpenCVLoader.InitDebug())
			{
				Log.Debug(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
				OpenCVLoader.InitAsync(OpenCVLoader.OpencvVersion300, this, mLoaderCallback);
			} else
			{
				Log.Debug(TAG, "OpenCV library found inside package. Using it!");
				mLoaderCallback.OnManagerConnected(LoaderCallbackInterface.Success);
			}
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			if (mOpenCvCameraView != null)
				mOpenCvCameraView.DisableView();
		}

		public void OnCameraViewStarted(int width, int height)
		{
			//TODO: Choose best resolution
			mOpenCvCameraView.SetResolution(1920, 1080);
			Log.Debug(TAG, $"OnCameraViewStarted");
		}

		public void OnCameraViewStopped()
		{
		}

		public Mat OnCameraFrame(CameraBridgeViewBase.ICvCameraViewFrame inputFrame)
		{
			return inputFrame.Rgba();
		}

		public bool OnTouch(View v, MotionEvent e)
		{
			mOpenCvCameraView.TakePicture();
			return false;
		}
	}

	class Callback: BaseLoaderCallback
	{
		private readonly MainActivity _activity;
		public Callback(Context context, MainActivity activity)
			: base(context)
		{
			_activity = activity;
		}
		public override void OnManagerConnected(int status)
		{
			switch (status)
			{
			case LoaderCallbackInterface.Success:
				{
					_activity.mOpenCvCameraView.EnableView();
					_activity.mOpenCvCameraView.SetOnTouchListener(_activity);
				}
				break;
			default:
				{
					base.OnManagerConnected(status);
				}
				break;
			}
		}
	}
}

