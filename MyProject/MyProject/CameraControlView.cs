﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Hardware;
using Android.Util;
using OpenCV.Android;
using Size = Android.Hardware.Camera.Size;

#pragma warning disable 0618

namespace MyProject
{
	public class CameraControlView: JavaCameraView, Camera.IPictureCallback
	{
		private const string TAG = "CameraControlView";

		bool canTakePicture = true;

		public CameraControlView(Context context, IAttributeSet attrs)
			: base(context, attrs)
		{
		}

		public List<Size> getResolutionList()
		{
			return MCamera.GetParameters().SupportedPreviewSizes.ToList();
		}

		public void SetResolution(int width, int height)
		{
			DisconnectCamera();
			MMaxHeight = height;
			MMaxWidth = width;
			ConnectCamera(Width, Height);
			var param = MCamera.GetParameters();
			param.SetPictureSize(width, height);
			MCamera.SetParameters(param);
		}

		public Size getResolution()
		{
			return MCamera.GetParameters().PreviewSize;
		}

		public void TakePicture()
		{
			Log.Info(TAG, "Taking picture");
			if (!canTakePicture) 
				return;
			MCamera.SetPreviewCallback(null);
			canTakePicture = false;
			MCamera.TakePicture(null, null, this);
		}

		public void OnPictureTaken(byte[] data, Camera camera)
		{
			Log.Info(TAG, "Picture taken. Processing");
			var props = camera.GetParameters();
			Log.Info(TAG, $"Camera props {props.PictureSize.Width}x{props.PictureSize.Height} {props.PreviewSize.Width}x{props.PreviewSize.Height}");

			// The camera preview was automatically stopped. Start it again.
			MCamera.StartPreview();
			MCamera.SetPreviewCallback(this);
			Intent showPictureIntent = new Intent(Context, typeof(PictureActivity));
			showPictureIntent.PutExtra("picture", data);
			Context.StartActivity(showPictureIntent);
			canTakePicture = true;
		}
	}
}