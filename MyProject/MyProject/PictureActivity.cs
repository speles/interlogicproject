﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenCV.Android;
using OpenCV.Core;
using OpenCV.ImgProc;
using ZXing;

namespace MyProject
{
	[Activity(Label = "PictureActivity",
	    ScreenOrientation = ScreenOrientation.Landscape,
		ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation)]
	public class PictureActivity: Activity
	{
		const string TAG = "MyProject::PictureActivity";
		public ImageView mTakenPictureView { get; private set; }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.PictureActivityLayout);

			mTakenPictureView = FindViewById<ImageView>(Resource.Id.processed_picture_view);

			byte[] data = Intent.Extras.GetByteArray("picture");
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.InMutable = true;
			Bitmap bmp = BitmapFactory.DecodeByteArray(data, 0, data.Length, options);

			Mat tmp = new Mat(bmp.Width, bmp.Height, CvType.CV_8UC(1));
			Utils.BitmapToMat(bmp, tmp);
			tmp = ProcessImage(tmp);
			Utils.MatToBitmap(tmp, bmp);

			//display processed image 
			mTakenPictureView.SetImageBitmap(bmp);
		}

		static double GetAngle(OpenCV.Core.Point pt1, OpenCV.Core.Point pt2, OpenCV.Core.Point pt0)
		{
			double dx1 = pt1.X - pt0.X;
			double dy1 = pt1.Y - pt0.Y;
			double dx2 = pt2.X - pt0.X;
			double dy2 = pt2.Y - pt0.Y;
			return (dx1 * dx2 + dy1 * dy2) / System.Math.Sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
		}

		private int CheckAnswers(int testId, List<KeyValuePair<int, int[]>> answers)
		{
			const string host = "speles.zapto.org:1234";
			var baseAddress = $"http://{host}/api/Tests/{testId}/Check";

			var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
			http.Accept = "application/json";
			http.ContentType = "application/json";
			http.Method = "POST";

			string serializedAnswers = JsonConvert.SerializeObject(answers.ToArray());
			System.Byte[] requestBytes = new ASCIIEncoding().GetBytes(serializedAnswers);

			Stream requestStream = http.GetRequestStream();
			requestStream.Write(requestBytes, 0, requestBytes.Length);
			requestStream.Close();

			var response = http.GetResponse();

			var responseStream = response.GetResponseStream();
			var sr = new StreamReader(responseStream);
			var responseContent = sr.ReadToEnd();

			var result = JsonConvert.DeserializeObject<ServerResponse>(responseContent);
			if (result.Message != null)
				throw new System.Exception(result.Message); else
				return result.Result;
		}

		private Mat PreprocessImage(Mat input)
		{
			Mat mGray = new Mat(input.Height(), input.Width(), CvType.Cv8uc4);
			Imgproc.CvtColor(input, mGray, Imgproc.ColorRgb2gray);

			//binarize image
			OpenCV.Core.Size blurSize = new OpenCV.Core.Size(3, 3);
			Imgproc.GaussianBlur(mGray, mGray, blurSize, 0);
			Imgproc.AdaptiveThreshold(mGray, mGray, 255, Imgproc.AdaptiveThreshMeanC, Imgproc.ThreshBinary, 75, 10);

			//for contours detection we need blank space = black
			OpenCV.Core.Core.Bitwise_not(mGray, mGray);

			//Imgproc.Resize(mGray, mGray, new OpenCV.Core.Size(mGray.Cols(), mGray.Rows()));
			return mGray;
		}

		private DocumentRectangle BuildRectangleTree(Mat input)
		{
			//Get contours
			JavaList<MatOfPoint> contours = new JavaList<MatOfPoint>();
			Mat hierarchy = new Mat();
			Imgproc.FindContours(input, contours, hierarchy, Imgproc.RetrTree, Imgproc.ChainApproxSimple);

			DocumentRectangle[] rectangleValues = new DocumentRectangle[contours.Count];
			List<DocumentRectangle> rectanglesList = new List<DocumentRectangle>();
			//Find rectangles
			for (int i = 0; i < contours.Count; i++)
			{
				//skip very small objects
				if (System.Math.Abs(Imgproc.ContourArea(contours[i])) < 80)
					continue;

				//aproximate contour
				MatOfPoint2f thisContour2f = new MatOfPoint2f();
				MatOfPoint approxContour = new MatOfPoint();
				MatOfPoint2f approxContour2f = new MatOfPoint2f();

				contours[i].ConvertTo(thisContour2f, CvType.CV_32FC(2));
				approxContour2f = thisContour2f;
				Imgproc.ApproxPolyDP(thisContour2f, approxContour2f, Imgproc.ArcLength(thisContour2f, true) * 0.03, true);
				approxContour2f.ConvertTo(approxContour, CvType.CV_32SC(2));

				//Rectangle contour should have at least
				if (approxContour.Rows() == 4 //4 angles
					&& System.Math.Abs(Imgproc.ContourArea(approxContour)) > 400 //big area
					&& Imgproc.IsContourConvex(approxContour)) //be convex
				{
					double maxCosine = 0;

					for (int j = 2; j < 5; j++)
					{
						OpenCV.Core.Point[] points = approxContour.ToArray();
						// find the maximum cosine of the angle between joint edges
						double cosine = System.Math.Abs(GetAngle(points[j % 4], points[j - 2], points[j - 1]));
						maxCosine = System.Math.Max(maxCosine, cosine);
					}

					// If cosines of all angles are small (angle -> 90), then it's rectangle
					if (maxCosine < 0.3)
					{
						DocumentRectangle docRectangle = new DocumentRectangle(approxContour);
						rectangleValues[i] = docRectangle;
						rectanglesList.Add(docRectangle);
					} else
					{
						Log.Debug("rect #2 fail", $"maxcos={maxCosine}");
					}
				} else
				{
					Log.Debug("rect #1 fail", $"rows = {approxContour.Rows()}, area = {System.Math.Abs(Imgproc.ContourArea(approxContour))}, isconv = {Imgproc.IsContourConvex(approxContour)}");
				}
			}

			if (rectanglesList.Count == 0) return null;

			//sort by area ascending, to determine father rectangle
			rectanglesList.Sort((x, y) => x.Area.CompareTo(y.Area));
			DocumentRectangle root = rectanglesList.Last();

			//build rectangle tree based on FindContours hierarchy
			int[] parentIds = new int[hierarchy.Cols()];
			for (int i = 0; i < hierarchy.Cols(); i++)
			{
				double[] data = hierarchy.Get(0, i);
				parentIds[i] = (int)data[3];
			}
			for (int i = 0; i < parentIds.Length; i++)
			{
				if (rectangleValues[i] != null)
				{
					int parent = parentIds[i];
					while (parent > 0)
					{
						if (rectangleValues[parent] != null)
						{
							rectangleValues[parent].Children.Add(rectangleValues[i]);
							break;
						}
						parent = parentIds[parent];
					}
				}
			}
			hierarchy.Release();

			//delete double-edged contours
			root = root.SkipDoubles();

			//delete children of squares
			root.ClearSquares();

			return root;
		}



		//Valid tree need to have
		//1 test number field
		//any count of questions with 4 possible answers
		private bool ValidateTree(DocumentRectangle root)
		{
			if (root == null) return false;
			if (root.Children.Count((rect) => rect.Children.Count == 0) != 1) return false;
			if (root.Children.Any((rect) => rect.Children.Count != 0 && rect.Children.Count != 4)) return false;
			return true;
		}

		private Mat ProcessImage(Mat input)
		{
			Mat mRgba = new Mat(input.Height(), input.Width(), CvType.Cv8uc4);
			Mat mGray = PreprocessImage(input);
			//Convert image that was used to parse data to RGBA for drawing on it
			Imgproc.CvtColor(mGray, mRgba, Imgproc.ColorGray2rgba, 4);

			DocumentRectangle root = BuildRectangleTree(mGray);
			int testId = 0;
			try
			{
				if (!ValidateTree(root)) throw new System.Exception();

				//Read test metadata
				var testNumberRect = root.Children.First(rect => rect.Children.Count == 0);

				Mat qrMat = mGray.Clone();

				//We need black text on white background
				Core.Bitwise_not(qrMat, qrMat);

				//Convert to bitmap
				Bitmap qrBitmap = Bitmap.CreateBitmap(qrMat.Cols(), qrMat.Rows(), Bitmap.Config.Argb8888);
				Utils.MatToBitmap(qrMat, qrBitmap);

				IBarcodeReader reader = new BarcodeReader();
				var result = reader.Decode(qrBitmap);

				bool isOrientedLeft = result.ResultPoints[0].X < qrMat.Cols() / 2;
				bool isOrientedTop = result.ResultPoints[0].Y < qrMat.Rows() / 2;
				Log.Debug(TAG, $"QR left={isOrientedLeft} top={isOrientedTop}");

				string qrText = result.Text;
				Log.Debug(TAG, $"Text = \"{qrText}\"");
				dynamic qrJson = JObject.Parse(qrText);
				testId = qrJson.test_id;
				Log.Debug(TAG, $"Test Id = {testId}");

				//Sort from left to right to restore questions order
				Comparison<DocumentRectangle> orientationComparison;

				//default qr in left bottom -> sort by x asc
				orientationComparison = (x, y) => x.Points[0].X.CompareTo(y.Points[0].X);

				if ((isOrientedLeft) && (isOrientedTop)) //qr in left top -> sort by y asc
					orientationComparison = (x, y) => x.Points[0].Y.CompareTo(y.Points[0].Y);

				if ((!isOrientedLeft) && (isOrientedTop)) //qr in right top -> sort by x desc
					orientationComparison = (x, y) => -1 * x.Points[0].X.CompareTo(y.Points[0].X);

				if ((!isOrientedLeft) && (!isOrientedTop)) //qr in right bottom -> sort by y desc
					orientationComparison = (x, y) => -1 * x.Points[0].Y.CompareTo(y.Points[0].Y);

				root.Children.Sort(orientationComparison);

				//Generate answers array
				List<KeyValuePair<int, int[]>> userTestAnswers = new List<KeyValuePair<int, int[]>>();
				int questionId = 0;
				foreach (var questionRect in root.Children)
				{
					if (questionRect.Children.Count != 4) continue;
					List<int> questionAnswers = new List<int>();
					int answerId = 0;
					foreach (var answerRect in questionRect.Children)
					{
						Mat imageMasked = answerRect.CutFromImage(mGray);
						double fillRate = ((double)Core.CountNonZero(imageMasked)) / answerRect.Area;
						answerRect.IsFilled = fillRate > 0.1;
						if (answerRect.IsFilled) questionAnswers.Add(answerId);
						answerId++;
					}
					userTestAnswers.Add(new KeyValuePair<int, int[]>(questionId, questionAnswers.ToArray()));
					questionId++;
				}
				try
				{
					int mark = CheckAnswers(testId, userTestAnswers);
					Toast.MakeText(this, $"You scored {mark} points", ToastLength.Short).Show();
				} catch (System.Exception e)
				{
					Toast.MakeText(this, $"Error: {e.Message}", ToastLength.Short).Show();
				}
			} catch
			{
				Toast.MakeText(this, $"Image was not recognised as valid form. Try again.", ToastLength.Long).Show();
			}

			if (root != null)
				DrawRectangle(mRgba, root);

			return mRgba;
		}

		private void DrawRectangle(Mat input, DocumentRectangle rect)
		{
			Scalar color = rect.IsFilled ? (new Scalar(0, 255, 0)) : (new Scalar(255, 0, 0));
			Imgproc.DrawContours(input, new JavaList<MatOfPoint> { rect.ToContour() }, 0, color, 2);
			foreach (var child in rect.Children)
				DrawRectangle(input, child);
		}

	}
}
 