﻿using System;
namespace MyProject
{
	public class ServerResponse
	{
		public string Message { get; set; }
		public int Result { get; set; }
	}
}
