﻿using System;
using System.Collections.Generic;
using Android.Runtime;
using OpenCV.Core;
using OpenCV.ImgProc;

namespace MyProject
{
	public class DocumentRectangle
	{
		public OpenCV.Core.Point[] Points { get; private set; }
		public double Area { get; private set; }
		public double Perimether { get; private set; }
		public bool IsFilled = false;
		public List<DocumentRectangle> Children = new List<DocumentRectangle>();
		public DocumentRectangle(MatOfPoint contour)
		{
			Points = contour.ToArray();
			Area = System.Math.Abs(Imgproc.ContourArea(contour));
			MatOfPoint2f thisContour2f = new MatOfPoint2f();
			contour.ConvertTo(thisContour2f, CvType.CV_32FC(2));
			Perimether = System.Math.Abs(Imgproc.ArcLength(thisContour2f, true));
		}

		public MatOfPoint ToContour()
		{
			return new MatOfPoint(Points);
		}

		public DocumentRectangle SkipDoubles()
		{
			if (Children.Count == 1)
			{
				//if area changes less than % then children is inner part of same edge
				if (Children[0].Area / Area > 0.6)
					return Children[0].SkipDoubles();
			} else
			{
				for (int i = 0; i < Children.Count; i++)
					Children[i] = Children[i].SkipDoubles();
			}
			return this;
		}

		//squares can't have any children under normal conditions
		public void ClearSquares()
		{
			double coef = Math.Pow(Perimether, 2) / Area;
			Android.Util.Log.Debug("ClearSquares", $"coef = {coef}, cnt = {Children.Count}");
			if (Math.Abs(coef - 16) < 0.1)
				Children.Clear();
			Children.ForEach(c => c.ClearSquares());
		}

		public Mat MaskOtherParts(Mat image)
		{
			MatOfPoint rectContour = ToContour();
			OpenCV.Core.Rect boundingBox = Imgproc.BoundingRect(rectContour);
			Mat mask = new Mat(image.Size(), CvType.CV_8UC(1));
			Imgproc.DrawContours(mask, new JavaList<MatOfPoint> { rectContour }, 0, new Scalar(255), -1);
			Mat imageMasked = new Mat();
			image.CopyTo(imageMasked, mask);
			return imageMasked;
		}

		public Mat CutFromImage(Mat image)
		{
			Mat masked = MaskOtherParts(image);
			OpenCV.Core.Rect boundingRect = Imgproc.BoundingRect(ToContour());
			return masked.Submat(boundingRect.Y + 5, boundingRect.Y + boundingRect.Height - 5, boundingRect.X + 5, boundingRect.X + boundingRect.Width - 5);
		}
	}
}
